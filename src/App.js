import './App.css';
import MultiSelect from './components/MultiSelect';

function App() {

    const data = [
        {
            id: '0',
            name: 'red'
        },
        {
            id: '1',
            name: 'blue'
        },
        {
            id: '2',
            name: 'green'
        },
        {
            id: '3',
            name: 'pink'
        },
        {
            id: '4',
            name: 'black'
        },
        {
            id: '5',
            name: 'brown'
        },
        {
            id: '6',
            name: 'blue'
        },
        {
            id: '7',
            name: 'orange'
        },
        {
            id: '8',
            name: 'white'
        },
        {
            id: '9',
            name: 'purple'
        },
        {
            id: '10',
            name: 'violet'
        },
        {
            id: '11',
            name: 'skyblue'
        },
        {
            id: '12',
            name: 'orangered'
        }
    ]

    return (
        <div>
            <MultiSelect data={data}/>
        </div>
    );
}

export default App;
