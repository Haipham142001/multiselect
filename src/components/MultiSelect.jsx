import React, { useState, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import OutsideClickHandler from 'react-outside-click-handler';

import './multi.scss';

const MultiSelect = (props) => {
    const [state, setState] = useState(props.data);
    const [listActive, setListActive] = useState([]);
    const [is, setIs] = useState(false);
    const [search, setSearch] = useState('');
    const [isActive, setIsActive] = useState(false);
    const input = useRef();
    const multi = useRef();
    const subMulti = useRef();

    const handleDeleteSeletect = (selected) => {
        const index = listActive.findIndex(select => {
            return select.id === selected.id;
        });
        let list = listActive;
        list.splice(index, 1);
        setListActive(list);
        let list2 = state;
        list2.push({
            id: selected.id,
            name: selected.name
        });
        list2.sort((a, b) => {
            return a.id - b.id
        })
        setState(list2);
        setIs(!is);
        focusInput();
    }

    const handleAddSelected = (selected) => {
        const list = state.filter((select) => {
            return select.id !== selected.id;
        })
        list.sort((a, b) => {
            return a.id - b.id
        })
        setState(list);
        let list2 = listActive;
        list2.push(selected);
        setListActive(list2);
        focusInput();
        setSearch('');
    }

    const handleClear = () => {
        let list = listActive;
        var list2 = list.concat(state);
        setState(list2);
        setListActive([]);
        focusInput();
        setSearch('');
    }

    const handleSearch = (text) => {
        setSearch(text);
    }

    const onKeyDownDelete = (e) => {
        if (JSON.stringify(listActive) !== '[]' && search === '') {
            if (e.keyCode === 8 || e.keyCode === 46) {
                let list = listActive;
                const select = list.pop();
                setListActive(list);
                const list2 = state;
                list2.push(select);
                list2.sort((a, b) => {
                    return a.id - b.id
                })
                setState(list2);
                setIs(!is);
            }
        }
    }

    const renderSubSelect = state.map((select, index) => {
        return <li key={index} onClick={() => handleAddSelected(select)}>
            {select.name}
        </li>
    });

    const renderSelected = listActive.map((select, index) => {
        return <li key={index}>
            {select.name}
            <div className='close-selected' onClick={() => handleDeleteSeletect(select)}>
                <FontAwesomeIcon icon={faTimes} />
            </div>
        </li>
    })

    const count = state.filter((select) => {
        return select.name.indexOf(search) !== -1
    })

    let renderSearch;

    if (JSON.stringify(count) !== '[]') {
        renderSearch = state.map((select, index) => {
            if (select.name.indexOf(search) !== -1) {
                return <li key={index} onClick={() => handleAddSelected(select)}>
                    {select.name}
                </li>
            }
            return null;
        })
    } else {
        renderSearch = <li><p>No options</p></li>
    }

    const focusInput = () => {
        input.current.focus();
    }


    return (
        <OutsideClickHandler onOutsideClick={() => setIsActive(false)}>
            <div
                className='multiselect active'
                ref={multi}
                onClick={() => setIsActive(true)}>
                <div className='main'>
                    <ul>
                        {renderSelected}
                        <input
                            type='text'
                            placeholder={JSON.stringify(listActive) === '[]'
                                ? 'Select...'
                                : ''}
                            ref={input}
                            value={search}
                            onChange={(e) => handleSearch(e.target.value)}
                            onKeyDown={(e) => onKeyDownDelete(e)}
                        />
                    </ul>

                </div>
                <div className='multi-icon'>
                    <div>
                        {JSON.stringify(listActive) !== '[]'
                            ? <FontAwesomeIcon
                                icon={faTimes}
                                onClick={handleClear} />
                            : ''}
                    </div>
                    <div>
                        <FontAwesomeIcon icon={faChevronDown} />
                    </div>
                </div>
                {isActive && <div className='sub-multiselect' ref={subMulti}>
                    {JSON.stringify(state) !== '[]'
                        ? <ul>
                            {search.length === 0
                                ? renderSubSelect
                                : renderSearch}
                        </ul>
                        : <p>No options</p>}
                </div>}
            </div>
        </OutsideClickHandler>
    )
}

export default MultiSelect
